package net.ethx.docker.jwr;

import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.JettyWebXmlConfiguration;
import org.eclipse.jetty.webapp.WebAppContext;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws Exception {
        final String root = args.length == 0 ? "." : args[0];
        final List<String> wars = Files.walk(Paths.get(root))
                                     .map(Path::toFile)
                                     .filter(File::isFile)
                                     .map(File::getAbsolutePath)
                                     .filter(n -> n.endsWith(".war"))
                                     .collect(Collectors.toList());

        if (wars.size() != 1) {
            System.err.println("Expected to find one WAR file to run, found " + wars);
            System.exit(-1);
        }

        //  create a server
        final Server server = new Server(8080);

        //  enable annotation parsing. jetty... this is insane.
        final Configuration.ClassList classes = Configuration.ClassList.setServerDefault(server);
        classes.addBefore(JettyWebXmlConfiguration.class.getName(), AnnotationConfiguration.class.getName());

        //  create the webapp context, and set it as the handler
        final WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/");
        webapp.setWar(wars.get(0));
        server.setHandler(webapp);

        //  fire it up
        server.start();
        server.join();
    }
}
